import os

import requests

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)


def test_hello_world():
    response = requests.get(URL + '/')
    assert response.text == 'Hello, World!'
    assert response.status_code == 200

def test_create():

    student_0 = {
        'name': 'Khaubula',
        'surname': 'Magomedov',
        'age': 19,
        'gender': 'non-binary'
    }

    student_error = {
        'gender': 'There are only two genders!'
    }

    response = requests.post(URL + '/create_student', json=student_0)
    assert response.status_code == 200
    assert response.json()['student_id'] >= 0
    assert response.json()['create'] == 'successfully'

    response = requests.post(URL + '/create_student', json=student_error)
    assert response.status_code == 400

def test_get():

    student_1 = {
        'name': 'Niyaz',
        'surname': 'Khabibulin',
        'age': 21,
        'gender': 'two-spirit'
    }

    result = requests.post(URL + '/create_student', json=student_1)
    id_student = result.json()['student_id']

    response = requests.get(URL + '/get_students')
    assert response.status_code == 200

    assert response.json()[str(id_student)] == student_1

def test_delete():

    student_1 = {
        'name': 'Niyaz',
        'surname': 'Khabibulin',
        'age': 21,
        'gender': 'two-spirit'
    }

    result = requests.post(URL + '/create_student', json=student_1)
    id_student = result.json()['student_id']

    response = requests.delete(URL + '/delete_student/' + str(id_student))
    assert response.status_code == 200

    response = requests.delete(URL + '/delete_student/' + str(id_student))
    assert response.status_code == 404

def test_put():

    student_1 = {
        'name': 'Niyaz',
        'surname': 'Khabibulin',
        'age': 21,
        'gender': 'two-spirit'
    }

    result = requests.post(URL + '/create_student', json=student_1)
    id_student = result.json()['student_id']

    response = requests.put(URL + '/update_student/' + str(id_student), json={'gender': 'male'})
    assert response.status_code == 200

    response = requests.put(URL + '/update_student/' + str(-2))
    assert response.status_code == 404


