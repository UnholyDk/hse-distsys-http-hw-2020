import os

from flask import Flask
from flask import request, jsonify

app = Flask(__name__)

class Student:
    def __init__(self, name, surname, age, gender):
        self.name = name
        self.surname = surname
        self.age = age
        self.gender = gender
        self.id = -1

    def __str__(self):
        return self.name

DATABASE = []

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/create_student', methods=['POST'])
def create_student():
    try:
        name = request.json['name']
        surname = request.json['surname']
        age = int(request.json['age'])
        gender = request.json['gender']

        student = Student(name, surname, age, gender)
        
        flag = False
        for i, x in enumerate(DATABASE):
            if x.id == -1:
                flag = True
                student.id = i
                DATABASE[i] = student
        
        if not flag:
            student.id = len(DATABASE)
            DATABASE.append(student)

        return jsonify({'student_id': student.id, 'create': 'successfully'}), 200
    except Exception as err:
        return jsonify({'error': str(err), 'create' : 'error'}), 400

@app.route('/get_students', methods=['GET'])
def get_students():
    result = {}
    for student in DATABASE:
        if student.id >= 0:
            result[student.id] = {
                'name': student.name,
                'surname': student.surname,
                'age': student.age,
                'gender': student.gender
            }
    return jsonify(result), 200

@app.route('/delete_student/<int:student_id>', methods=['DELETE'])
def delete_student(student_id):
    if 0 <= student_id < len(DATABASE):
        student = DATABASE[student_id]
        if student.id == student_id:
            student.id = -1
            return jsonify({'delete': 'successfully'}), 200
    return jsonify({'error': 'Student with given id not found'}), 404

@app.route('/update_student/<int:student_id>', methods=['PUT'])
def update_student(student_id):
    if 0 <= student_id < len(DATABASE):
        student = DATABASE[student_id]
        if student.id != -1:
            if 'name' in request.json:
                student.name = request.json['name']
            if 'surname' in request.json:
                student.surname = request.json['surname']
            if 'age' in request.json:
                student.age = int(request.json['age'])
            if 'gender' in request.json:
                student.gender = request.json['gender']
            DATABASE[student_id] = student
            return jsonify({'update': 'successfully'}), 200
    return jsonify({'error': 'Student with given id not found'}), 404


app.run(host='0.0.0.0', port=int(os.environ.get('HSE_HTTP_FLASK_PORT', 80)))
