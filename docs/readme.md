# Документация на мой API

## Описание сущности:
Класс - Студент, имеет 4 поля <br>
`name` - string <br>
`surname` - string <br>
`age` - int <br>
`gender` - string <br>

## Endpoints
### **POST /create_student**
Создать студента, в body нужно передать 4 поля студента <br>
Если студент создался успешно, то возвращается
`{'student_id': id_student, 'create': 'successfully'}`
и статус `200` <br>
Иначе `{'error': str(err), 'create' : 'error'}` и статус `400`, где `str(err)` - Ошибка, которую выкинул python.
### **GET /get_students**
Возвращается список студентов <br>
Example: <br>
```json
{
    "0":
        {
            "age": 19,
            "gender": "non-binary",
            "name": "Khaubula",
            "surname": "Magomedov"
        },
    "1":
        {
            "age": 21,
            "gender": "two-spirit",
            "name": "Niyaz",
            "surname": "Khabibulin"
        }
}
```
### **DELETE /delete_student/{studentId}**
Удалить студента с данным `id` <br>
Если студент с данным `id` существует, то на сервере данный студент удаляется и присылается `{'delete': 'successfully'}` и статус `200` <br>
Иначе присылается `{'error': 'Student with given id not found'}` и статус `404`

### **PUT /update_student/{studentId}**
Обновить поля студента с данным `id`, в body передается поля студента, которые нужно изменить <br>
В случае успеха, присылается `{'update': 'successfully'}` и статус `200` <br>
Иначе, `{'error': 'Student with given id not found'}` и статус `404`